#include <stdio.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <assert.h>

#define SOCKET_PATH "\0hidden"
// #define SOCKET_PATH "source/socket_test"
#define NO_FLAGS_SPECIFIED 0
#define ERROR 0

int main() {
    
    // Creating the socket (file descriptor) 
    int sending_socket = socket(AF_UNIX, SOCK_STREAM, 0);

    if (sending_socket == ERROR)
    {
       perror("Client: socket couldn't be created\n");
       exit(1);
    }

    // Create and initialize soccaddr_un that defines the path
    // to the special file that is the socket
    struct sockaddr_un socket_address;
    memset(&socket_address, 0, sizeof(socket_address));
    socket_address.sun_family = AF_UNIX;
    strncpy(socket_address.sun_path,
            SOCKET_PATH,
            sizeof(socket_address.sun_path)-1);
 
    int connection_result;
    do
    {
        connection_result = connect(sending_socket,
                            (struct sockaddr*)& socket_address,
                            sizeof(socket_address));
        if (connection_result == ERROR)
            perror("Client: connection error\n");

    } while(connection_result == ERROR);

    printf("Client: connected\n");

    // Build the messages to send
    const int TOTAL_MESSAGES = 5;
    const char * messages[TOTAL_MESSAGES];
    messages[0] = "1-Hello world";
    messages[1] = "2-Hello world";
    messages[2] = "3-Hello world";
    messages[3] = "4-Hello world";
    messages[4] = "5-Hello world";

    unsigned int num_of_messages_sent = 1;
    while(num_of_messages_sent <= TOTAL_MESSAGES)
    {
        const char * message = messages[num_of_messages_sent-1];
        printf("Sending message #%u: %s\n", num_of_messages_sent, message);

        if (send(sending_socket,
                 message,
                 strlen(message),
                 NO_FLAGS_SPECIFIED) == ERROR)
        {
            perror("Client: error while sending the message");
            break;
        }

       ++num_of_messages_sent;  
    }
    
    return 0;
}
