#include <stdio.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <stdlib.h>

#define SOCKET_PATH "\0hidden"
// #define SOCKET_PATH "source/socket_test"
#define MAX_MESSAGE_QUEUE 5
#define NO_FLAGS_SPECIFIED 0
#define ERROR -1
#define MESSAGE_BUFFER_SIZE 100

int main()
{    
    // Creating the socket (file descriptor) 
    int listening_socket = socket(AF_UNIX, SOCK_STREAM, 0);

    if (listening_socket == ERROR)
    {
       perror("Server: socket couldn't be created\n");
       exit(1);
    }

    // Create and initialize soccaddr_un that defines the path
    // to the special file that is the socket
    struct sockaddr_un socket_address;
    memset(&socket_address, 0, sizeof(socket_address));
    socket_address.sun_family = AF_UNIX;
    strncpy(socket_address.sun_path,
            SOCKET_PATH,
            sizeof(socket_address.sun_path)-1);

    // Remove the socket if it already exists before bind()
    // You will get an EINVAL error if the file is already here
    unlink(SOCKET_PATH);
    
    // bind() assigns the address specified by sockaddr to the
    // socket referred to by the file descriptor listening_socket
    if (bind(listening_socket,
             (struct sockaddr*)&socket_address,
             sizeof(socket_address)) == ERROR)
    { 
        perror("Server: error during binding\n");
        exit(1);
    }

    // Mark the socket described by the listening_socket as a 
    // passive socket (ie: will be used to accept incoming 
    // connection requests)
    if (listen(listening_socket, MAX_MESSAGE_QUEUE) == ERROR)
    { 
        perror("Server: error during listening\n");
        exit(1);
    }
    
    while (true)
    {
        printf("Server: waiting for a connection ...\n");
       
        // Extracts the first connection request on the queue of
        // pending connections for the listening_socket, creates
        // a new connected socket, and returns a new file descriptor
        // referring to that socket 
        int connected_socket = accept(listening_socket,NULL,NULL);
        if (connected_socket == ERROR) 
        {
            perror("Server: accept error");
            continue;
        }

        printf("Server: connected\n");


        ssize_t num_of_bytes_received;
        char message_buffer[MESSAGE_BUFFER_SIZE];

        while (true)
        {
            num_of_bytes_received = recv(connected_socket,
                                         message_buffer,
                                         sizeof(message_buffer),
                                         NO_FLAGS_SPECIFIED);
            if (num_of_bytes_received <= 0) 
            {
                // Error
                if (num_of_bytes_received < 0)
                    perror("Server: read error");
                // peer shutdown
                else
                    printf("EOF\n");
                
                close(connected_socket);
                break;
            }

            printf("Server: read %ld bytes: ", num_of_bytes_received);
            for (int i=0; i<num_of_bytes_received; ++i) {
                printf("%c",message_buffer[i]);
            }
            printf("\n");
        }

    }

    return 0;
}
