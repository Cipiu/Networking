#! /bin/bash

echo 

echo "Launching the server process"
./build/server &
echo "Server process launched with pid $!"

echo "Launching the client process"
./build/client &
echo "Client process launched with pid $!"

echo
