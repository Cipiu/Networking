#Unix Domain Socket

* To compile and run without CMake: <br />
g++-4.8 -std=c++11 [FileName].cc -o [FileName] <br />

* To compile using CMake: <br />
cmake . <br />
make <br />

* To run <br />
./build/[FileName] <br />

* To run using launch.sh <br />
./launch.sh & <br />

* Docs <br />
http://troydhanson.github.io/network/Unix_domain_sockets.html <br />
http://beej.us/guide/bgipc/output/html/multipage/unixsock.html <br />
https://upsilon.cc/~zack/teaching/1314/progsyst/cours-09-socket-unix.pdf <br />
http://www.boost.org/doc/libs/1_40_0/doc/html/boost_asio/overview/posix/local.html <br />
https://pymotw.com/2/socket/uds.html <br />
